module gitlab.com/josselin1/luabox

go 1.22

require (
	github.com/Shopify/go-lua v0.0.0-20221004153744-91867de107cf
	github.com/markbates/pkger v0.17.1
	go.uber.org/zap v1.27.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/gobuffalo/here v0.6.0 // indirect
	go.uber.org/multierr v1.10.0 // indirect
)
